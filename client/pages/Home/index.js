import { React, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Typography, TextField, Button} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';

import AppHelper from '../../app-helper'
import moment from 'moment'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        width: '50%',
        margin: '30px auto',
        [theme.breakpoints.down('md')]: {
            width: '100%',
            padding: '10px'
        },
        '& .MuiOutlinedInput-input': {
            padding: '10px',
            fontSize: '12px'
        },
        '& .MuiInputBase-root': {
            '& fieldset': {
            },
            '&.Mui-focused fieldset': {
                border: '1px solid gray',
            }
        },
        "& input::-webkit-clear-button, & input::-webkit-outer-spin-button, & input::-webkit-inner-spin-button": {
            display: "none",
            margin: 80
        },
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        boxShadow: 'none'
    },
    Title: {
        padding: '20px 0'
    },
    Input: {
        marginRight: "5px",
        [theme.breakpoints.down('md')]: {
            margin: '0',
            marginBottom: '7px'
        },
    },
    Result: {
        border: '1px solid #aaaaaa',
        borderRadius: '6px',
        textAlign: 'left',
        margin: '10px 0',
        padding: '10px'
    },
    Button: {
        marginTop: '10px'
    },
    Table:{
        fontSize: '12px',
        padding: '5px',
        border: '1px solid #aaaaaa',
        height: '30px'
    },
    TableHeaderContainer:{
        padding: '0 25px 0 10px',
        [theme.breakpoints.down('md')]: {
            padding: '0 10px'
        },
    },
    TableHeader:{
        fontSize: '12px',
        padding: '5px 0',
        border: '1px solid #aaaaaa',
        height: '30px',
        textAlign: 'center'
    },
    TableRaw:{
        fontSize: '12px',
        padding: '10px',
        '& .MuiSelect-select':{
            fontSize: '12px'
        }
    },
    ResultHolder:{
        color: theme.palette.text.secondary,
        boxShadow: 'none',
        padding: '5px 0'
    },
    ResultContainer:{
        height: '200px',
        overflowY: 'scroll',
        padding: '0 10px'
    },
}));

export default function CenteredGrid() {
    const classes = useStyles();

    const [start, setStart] = useState('')
    const [end, setEnd] = useState('')
    const [result, setResult] = useState(0)
    const [isActive, setIsActive] = useState(false)
    const [data,setData] = useState([])
    const [status,setStatus] = useState(false)
    const [generated,setGenerated] =useState('')
    const [date,setDate] =useState([])
    const [selectDate,setSelectDate] =useState([])
    const [filterDate,setFilterDate] =useState([])
    const [dateNow,setDateNow] = useState(moment(Date()).format('MMMM DD YYYY')) 

    const handleChange = (event) => {
      setDateNow(event.target.value);
    };
 
    //Reset all input
    const Reset = () => {
        setResult(0)
        setStart('')
        setEnd('')
        setGenerated(0)
    }
    //Set all input
    const Generate = (e) => {
        e.preventDefault()
        setStatus(false)
        setGenerated(result)// final score generated    
        //call the function for fetching data "POST"
        AddScore()
    }
    // Function to be call fetch to databae
    const AddScore = () =>{
        // Adding Score
        fetch(`${AppHelper.API_URL}/addScore`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Score: result,
                Count: data.filter(res => {
                    return res.Scores === result
                }).length+1,
                Date: new Date(),
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                setStatus(true)
            }
        })
        //Updating count
        fetch(`${AppHelper.API_URL}/updateCount`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Score: result,
                Count: data.filter(res => {
                    return res.Scores === result
                }).length+1,
              
            })
        })
        .then(res => res.json())
        .then(data => {})
    }
    useEffect(() => {
        if (start !== "" && end !== "") {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
        fetch(`${AppHelper.API_URL}/details`,)
        .then(res => res.json())
        .then(result => {
            setData(result.reverse())
        })
        // calling a function to generate a random number given by input
        setResult(getRandomInt(parseInt(start),parseInt(end)))
        // Getting all the date and put it in the date array
        setDate(data.map(res => {
            return moment(res.Date).format('MMMM DD YYYY')
        }))
        // Removing duplicate date date array
        const filterDate = [...new Set(date)]
        //assigning the selectDate from filterData to be used in dropdown
        setSelectDate(filterDate)
        // filtering the date thata re equal to current date to be display in current history
        setFilterDate(data.filter(res => {
            return moment(res.Date).format('MMMM DD YYYY') === dateNow
        }))
    },[data,generated])


    //Random score generator function    
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
      }

    return (
        <div className={classes.root}>
            <Grid container>
                <Grid item xs={12}>
                    <Paper className={classes.paper}>
                        <Typography variant="h5" className={classes.Title}>
                            Random Score Generator
                        </Typography>
                        <form onSubmit={(e) => Generate(e)}>
                            <Grid container
                                direction="row"
                                justify="center"
                                alignItems="center">
                                <Grid item xs={12} sm={4} className={classes.Input}>
                                    <TextField required id="From" variant="outlined" placeholder="Input a starting number" type="number" value={start} onChange={e => setStart(e.target.value)} fullWidth />
                                </Grid>
                                <Grid item xs={12} sm={4}>
                                    <TextField required id="To" variant="outlined" placeholder="Input a ending number" type="number" value={end} onChange={e => setEnd(e.target.value)} fullWidth />
                                </Grid>
                            </Grid>
                            {isActive ?
                            <Button className={classes.Button} variant="contained" color="primary" type="submit" id="submitBtn">
                                Generate
                            </Button>
                            :
                            <Button disabled className={classes.Button} variant="contained" color="primary" type="submit" id="submitBtn">
                                Input a number
                            </Button>
                            }
                        </form>
                    </Paper>
                    <Grid container direction="row"
                        justify="center"
                        alignItems="center">
                        <Button className={classes.ButtonReset} variant="contained" color="secondary" onClick={e => Reset(e)}>
                            Reset
                        </Button>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid
                    className={classes.Result}
                    container
                    direction="row"
                    justify="center"
                    alignItems="center">
                        {generated === '' ? <Typography variant='body1'>0</Typography> : 
                            <Typography>{status ? generated : ".........."}</Typography>
                        }
                    </Grid>
                    <Grid container
                    direction="row"
                    justify="flex-start"
                    alignItems="center"
                    className={classes.TableRaw}>
                        <Grid item xs={5}><Typography variant="body1">Result History</Typography></Grid>
                        <Grid item xs={7}>
                        <TextField
                        fullWidth
                        id="date"
                        select
                        onChange={handleChange}
                        SelectProps={{
                            native: true,
                          }}
                          helperText="Select a date">
                        {selectDate.map((option) => (
                            <option key={option} value={option} className={classes.option}>
                                {option}
                            </option>
                        ))}
                        </TextField>
                        </Grid>
                    </Grid>
                    
                    <Grid container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    className={classes.TableHeaderContainer}>
                        <Grid item xs={2}> <Typography variant="body1" className={classes.TableHeader}>Score</Typography></Grid>
                        <Grid item xs={4}> <Typography variant="body1" className={classes.TableHeader}>Generated per day</Typography></Grid>
                        <Grid item xs={6}> <Typography variant="body1" className={classes.TableHeader}>Date generated</Typography></Grid>
                    </Grid>
                    <Grid item className={classes.ResultContainer}>
                    {filterDate.map(res => {
                        return (
                            <Grid container
                            direction="row"
                            justify="flex-start"
                            alignItems="center"
                            key={res._id} >
                                <Grid item xs={2}> <Typography variant="body1" className={classes.Table}>{res.Scores}</Typography></Grid>
                                <Grid item xs={4}> <Typography variant="body1" className={classes.Table}>{res.Count}</Typography></Grid>
                                <Grid item xs={6}> <Typography variant="body1" className={classes.Table}>{moment(res.Date).format('MMMM DD YYYY HH:mm')}</Typography></Grid>
                            </Grid>
                        )
                    })}
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

