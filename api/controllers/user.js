const { updateMany } = require("../models/user")
const User = require("../models/user")

//get all the data
module.exports.get = () => {
	return User.find({}).then(resultFromFindById => {
		return resultFromFindById
	})
}
//Adding generated Score
module.exports.add = (params) => {
	let newUser = new User({
		Scores: params.Scores,
		Date: params.Date,
	})
	return newUser.save().then((resultFromFindOne, err) => {
		return err ? false : true
	});
}

//Updating count
module.exports.update = (params) => {
	return User.find({Scores: params.Scores}).then(res => {
		let newCount = {
			Count: params.Count
		}
		return User.updateMany({'Scores': params.Scores},newCount).then((res,err) => {
			return err ? false : true
		})
	})
}
