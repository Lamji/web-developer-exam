const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
		Scores: {
			type: Number,
		},
		Count: {
			type: Number,
		
		},
		Date: {
			type: Date,
			default: new Date()
		}
})

module.exports = mongoose.model("Details", userSchema)