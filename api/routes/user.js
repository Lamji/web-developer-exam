const express = require("express")
const router = express.Router()
const UserController = require("../controllers/user")


//auth.verify ensures that a user is logged in before proceeding to the next part of the code
router.get('/details', (req, res) => {
	//we use the id of the user from the token to search for the user's information
	UserController.get().then(user => res.send(user))
})

//create a new course
router.post('/addScore', (req, res) => {
	const params = {
        Scores: req.body.Score,
        Date: req.body.Date,
        Count: req.body.Count
    }
    UserController.add(params).then(result => res.send(result))
})

//updating count
router.post('/updateCount', (req, res) => {
	const params = {
        Scores: req.body.Score,
        Count: req.body.Count
    }
    UserController.update(params).then(result => res.send(result))
})

module.exports = router